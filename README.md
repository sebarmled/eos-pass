  EOSPass Server
================

A PHP demonstration providing a server-side implementation of the EOSPass authentication protocol.

Requires PHP 7 (tested on v7.1)

Needs EOSCheckSig in the document root of PHP server . If php files are in the /dir/dir/dir/html directory, the EOSCheckSig Python script files must be in /dir/dir/dir/EOSCheckSig

3 web pages, using style.css:
* index.php: the home page with the login button
* user.php: the user's space
* logout.php: ends the user session, directly redirects to index

A POST endpoint login.php to get and analyze the login query containing the signature.

eospass.php : the EOSPass php library tools


License :
----------

 EOSPass Server (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential

