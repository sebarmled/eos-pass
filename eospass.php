<?php

/*
 EOSPass Server eospass.php (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

// a PHP Library for EOSPass

function extractNonce( $uri ) {
	if(preg_match('/(&|\?)x=([^&]+)/', $uri.'&', $match)) {
		return $match[2];
	}
	return '';
};
function makeURI( $nonce ){
	$srvdom = $_SERVER['SERVER_NAME'];
	if ( preg_match('/^[-,a-zA-Z0-9]{20,48}$/', $nonce, $CleanNonce) )
		return "eospass://$srvdom/login.php?x=".$CleanNonce[0];
	else return "XXX?XXX";
};
function checkSig( $sig , $msg, $pubkey ){
	if ( substr($sig, 0, 7) === "SIG_K1_" ){
		// filter and sanitize inputs
		$pubkey_re = '/^EOS[1-9A-HJ-NP-Za-km-z]{49,54}$/';
		$sig_re = '/^SIG_K1_[1-9A-HJ-NP-Za-km-z]{92,98}$/';
		$srvdom = $_SERVER['SERVER_NAME'];
		$uri_re = "/^eospass:\/\/$srvdom\/login.php\?x\=[-,a-zA-Z0-9]{20,48}$/";
		preg_match($uri_re, $msg, $uri_clean);
		preg_match($pubkey_re, $pubkey, $pubkey_clean);
		preg_match($sig_re, $sig, $sig_clean);
		// check anything left after the filtering
		if( !isset($uri_clean[0]) || !isset($pubkey_clean[0]) || !isset($sig_clean[0]) )
			return false;
		// check the signature with EOSCheckSig
		$sigOK = exec("python $_SERVER[DOCUMENT_ROOT]/../EOSCheckSig/eoschecksig.py $uri_clean[0] $pubkey_clean[0] $sig_clean[0]");
		return $sigOK == 'GOOD';
	}
	else
		// only check sig K1 type
		return false;
};
?>
