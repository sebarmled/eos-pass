<?php

/*
 EOSPass Server index.php (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

session_start();
// nonce is session ID, refreshed every 30 seconds
$nonce_time = 30;
if(!isset($_SESSION["nonce_valid"])){
	$_SESSION['nonce_valid'] = time() + $nonce_time;
}
$user = isset( $_SESSION['user']) ?  $_SESSION['user'] : '';
if (!$user){
	if ($_SESSION['nonce_valid'] <= time()){
		session_regenerate_id(true);
		$_SESSION['nonce_valid'] = time() + $nonce_time;
	}
}
$ssid = session_id();
?>
<!doctype html>
<html>
	<head>
		<title>EOSPass demo</title>
		<meta name="description" content="EOSPass login system demo server">
		<meta name="keywords" content="blockchain,EOS,login,web">
		<link rel="stylesheet" href="style.css"> 
	</head>
<body>
<?php
	if ($user) print('<div class="logon">');
	else print('<div class="logoff">');
?>
	<h1 style="text-align: center;">EOSPass Server demo</h1>
	<p style="text-align: right;">
	<?php
		if ($user) print('<a href="user.php">'.$user.'</a>');
		else print('<br>');
		?>
	</p>
	<hr />
	</div>
	<p style="text-align: right;">&nbsp;</p>
	<p style="text-align:center" class=
	<?php
		if ($user) print('"txtlogon">You are logged in.</p>');
		else print('"txtlogoff">You are not logged in.</p>
			<p style="text-align: center;">
			<a href="eospass://'.$_SERVER['SERVER_NAME'].'/login.php?x='.$ssid.'"><button>LOGIN</button></a>');
	?>
	</p>
</body>
</html>
