<?php

/*
 EOSPass Server login.php (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

// a POST endpoint to analyze an EOSPass login query

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	session_start();
	require_once dirname(__FILE__) . "/eospass.php";
	// Check nonce is still valid
	if ($_SESSION['nonce_valid'] < time()){
		header("HTTP/1.0 401 Unauthorized");
		header('Content-Type: application/json');
		$datar = ['message' => 'Nonce is not valid (expired or unknown)'];
		print(json_encode($datar));
		exit();
	}
	// read POST data
	$data = json_decode(file_get_contents('php://input'), true);
	// query the EOS blockchain to get the eospass account address
	$url = 'https://proxy.eosnode.tools/v1/chain/get_account';
	$account_name = $data["account_name"];
	$datap = array('account_name' => $account_name);
	$options = array(
		'http' => array(
			'header'  => "Content-type: application/json\r\n",
			'method'  => 'POST',
			'content' => json_encode($datap)
		)
	);
	$context  = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	// account_name exists ?
	if ($result == false){
		header("HTTP/1.0 401 Unauthorized");
		header('Content-Type: application/json');
		$datar = ['message' => 'Account doesn\'t exist'];
		print(json_encode($datar));
		exit();
	}
	else {
		$ssid = session_id();
		$uri = $data["uri"];
		$sig = $data["signature"];
		$dataarr = json_decode($result);
		// Scan all the account keys
		foreach( $dataarr->permissions as $key){
			if ($key->perm_name === "eospass"){
				$eosaddr = $key->required_auth->keys[0]->key;
				if (// Check the nonce is the good one
					extractNonce($uri) === $ssid &&
					// Check URI looks good
					makeURI($ssid) === $uri &&
					// Check URI signed with eospass address
					checkSig($sig, $uri, $eosaddr)
				)	{
						// User is granted and session is registered
						$_SESSION['user'] = $account_name;
						exit();
				}
				else { // User is not allowed
					header("HTTP/1.0 401 Unauthorized");
					header('Content-Type: application/json');
					$datar = ['message' => 'signature check failed'];
					print(json_encode($datar));
					exit();
				}
			}
		}
		// No eospass key found for this user
		header("HTTP/1.0 401 Unauthorized");
		header('Content-Type: application/json');
		$datar = ['message' => 'No eospass key for this account'];
		print(json_encode($datar));
		exit();
	}
}
// elseif ($_SERVER['REQUEST_METHOD'] === 'GET') {
	// print("EOSPass Callback");
	// //to login.php or to user.php whether logged in
	// return;
// }
else {
	die(header("HTTP/1.0 405 Method Not Allowed"));
}
?>
