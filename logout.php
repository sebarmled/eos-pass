<?php

/*
 EOSPass Server logout.php (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

// logout page which disconnects and clears all the user session data
session_start();
$user = isset( $_SESSION['user']) ?  $_SESSION['user'] : '';
if ($user){
	unset($_SESSION['user']);
	session_unset();
	session_destroy();
	session_write_close();
	setcookie(session_name(),'',0,'/');
}
// redirect to index
header('Location: index.php');
?>
