<?php

/*
 EOSPass Server user.php (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

// user space web page

?>
<!doctype html>
<html>
	<head>
		<title>EOSPass demo</title>
		<meta name="description" content="EOSPass login system demo server">
		<meta name="keywords" content="blockchain,EOS,login,web">
		<link rel="stylesheet" href="animate.min.css">
		<link rel="stylesheet" href="style.css">
	</head>
<body>
<?php
	session_start();
	$user = isset( $_SESSION['user']) ?  $_SESSION['user'] : '';
	if ($user) print('<div class="logon">');
	else print('<div class="logoff">');
?>
	<h1 style="text-align: center">EOSPass Server demo</h1>
	<?php
		if (!$user){
			header('HTTP/1.0 401 Unauthorized');
			print('<hr /></div> 401 Unauthorized <br><br><a href="index.php"> &gt; login </a></body></html>');
			die();
		}
	?>
	<p style="text-align: right">user : <?php print($user) ?></p>
	<hr />
	</div>
	<p style="text-align: right">&nbsp;</p>
	<p class="animated jackInTheBox delay-1s fast txtlogon" style="text-align:center;font-size:24px">Welcome <?php print($user) ?></p><br>
	<div style="text-align: center">
	<?php
		if ($user){
			print('<form action="logout.php"><button type="submit">Logout</button></form>');
		}
	?>
	</div>
</body>
</html>
